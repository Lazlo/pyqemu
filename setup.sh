#!/bin/bash

pkgs=""
pkgs="$pkgs make"
pkgs="$pkgs python3"
pkgs="$pkgs python3-pip"

apt-get -q install -y $pkgs
pip3 install --system -r requirements.txt
