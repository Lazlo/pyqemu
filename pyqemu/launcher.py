import subprocess

class Launcher():

	def __init__(self):
		self.default_cmd_bin = "/usr/bin/qemu-system-x86_64"
		self.default_cmd_args = ["-S", "-monitor", "stdio"]
		self.cmd_bin = self.default_cmd_bin
		self.cmd_args = self.default_cmd_args
		self.proc = None

	def get_cmd(self):
		cmd = [self.cmd_bin]
		cmd.extend(self.cmd_args)
		return cmd

	def set_cmd_bin(self, path):
		self.cmd_bin = path

	def get_cmd_args(self):
		return self.cmd_args

	def add_cmd_args(self, args):
		self.cmd_args.extend(args)

	def launch(self):
		cmd = self.get_cmd()
		self.proc = subprocess.Popen(cmd, stdin=subprocess.PIPE)

	def monitor(self, mon_cmd):
		self.proc.stdin.write(mon_cmd.encode())
		self.proc.stdin.flush()

	def cont(self):
		self.monitor("cont\r")

	def stop(self):
		self.monitor("stop\r")

	def quit(self):
		self.monitor("quit\r")
