#!/usr/bin/python3

import unittest
import launcher
import subprocess

stdin_write_called = False
stdin_write_arg1 = None
stdin_flush_called = False

class FakeSubprocessPopenStdin():

	def __init__(self):
		global stdin_write_called
		global stdin_write_arg1
		global stdin_flush_called
		stdin_write_called = False
		stdin_write_arg1 = None
		stdin_flush_called = False

	def write(self, arg1):
		global stdin_write_called
		global stdin_write_arg1
		stdin_write_called = True
		stdin_write_arg1 = arg1

	def flush(self):
		global stdin_flush_called
		stdin_flush_called = True

popen_called = False
popen_args = []

class FakeSubprocessPopen():

	def __init__(self, arg1, **kwargs):
		global popen_called
		global popen_args
		popen_called = True
		popen_args.append(arg1)
		popen_args.append(kwargs)
		self.stdin = FakeSubprocessPopenStdin()

class LauncherGetCmdTestCase(unittest.TestCase):

	def setUp(self):
		self.q = launcher.Launcher()

	def test_get_cmd_returns_default_cmd(self):
		expected_cmd = ["/usr/bin/qemu-system-x86_64", "-S", "-monitor", "stdio"]
		self.assertEqual(expected_cmd, self.q.get_cmd())

class LauncherSetCmdBinTestCase(unittest.TestCase):

	def setUp(self):
		self.q = launcher.Launcher()

	def test_set_cmd_bin(self):
		cmd_bin_passed = "/usr/local/src/qemu/x86_64-softmmu/qemu-system-x86_64"
		expected = cmd_bin_passed
		self.q.set_cmd_bin(cmd_bin_passed)
		actual = self.q.get_cmd()[0]
		self.assertEqual(expected, actual)

class LauncherGetCmdArgsTestCase(unittest.TestCase):

	def setUp(self):
		self.q = launcher.Launcher()

	def test_get_cmd_args_returns_default_args(self):
		expected_default_args = ["-S", "-monitor", "stdio"]
		self.assertEqual(expected_default_args, self.q.get_cmd_args())

class LauncherAddCmdArgsTestCase(unittest.TestCase):

	def setUp(self):
		self.q = launcher.Launcher()

	def test_add_cmd_args_extends_existing_args(self):
		args_passed = ["-k", "de"]
		expected = ["-S", "-monitor", "stdio"]
		expected.extend(args_passed)
		self.q.add_cmd_args(args_passed)
		self.assertEqual(expected, self.q.get_cmd_args())

class LauncherLaunchTestCase(unittest.TestCase):

	def setUp(self):
		self.q = launcher.Launcher()

	#
	# Qemu Process related tests
	#

	def test_launch_calls_subprocess_Popen(self):
		global popen_called
		popen_called = False
		subprocess.Popen = FakeSubprocessPopen
		self.q.launch()
		self.assertEqual(True, popen_called)

	def test_launch_calls_subprocess_Popen_with_frist_arg_cmd_from_get_cmd(self):
		global popen_args
		popen_args = []
		expected = self.q.get_cmd()
		subprocess.Popen = FakeSubprocessPopen
		self.q.launch()
		self.assertEqual(expected, popen_args[0])

	def test_launch_calls_subprocess_Popen_with_second_kwarg_stdin_pipe(self):
		global popen_args
		popen_args = []
		expected = {'stdin': subprocess.PIPE}
		subprocess.Popen = FakeSubprocessPopen
		self.q.launch()
		self.assertEquals(expected, popen_args[1])

class LauncherMonitorTestCase(unittest.TestCase):

	def setUp(self):
		self.q = launcher.Launcher()

	def test_monitor_calls_subprocess_stdin_write(self):
		global stdin_write_called
		stdin_write_called = False
		subprocess.Popen = FakeSubprocessPopen
		self.q.launch()
		self.q.monitor("cont\r")
		self.assertEqual(True, stdin_write_called)

	def test_monitor_calls_subprocess_stdin_write_with_bytes_arg_passed_to_monitor(self):
		global stdin_write_arg1
		stdin_write_arg1 = None
		cmd = "info\r"
		expected = cmd.encode()
		subprocess.Popen = FakeSubprocessPopen
		self.q.launch()
		self.q.monitor(cmd)
		self.assertEqual(expected, stdin_write_arg1)

	def test_monitor_calls_subprocess_stdin_flush(self):
		global stdin_flush_called
		stdin_flush_called = False
		cmd = "random-invalid-cmd\r"
		subprocess.Popen = FakeSubprocessPopen
		self.q.launch()
		self.q.monitor(cmd)
		self.assertEqual(stdin_flush_called, True)

class LauncherContTestCase(unittest.TestCase):

	def setUp(self):
		self.q = launcher.Launcher()

	def test_cont_calls_subprocess_stdin_write_with_cont_str(self):
		global stdin_write_arg1
		stdin_write_arg1 = None
		expected = "cont\r".encode()
		subprocess.Popen = FakeSubprocessPopen
		self.q.launch()
		self.q.cont()
		self.assertEqual(expected, stdin_write_arg1)

class LauncherStopTestCase(unittest.TestCase):

	def setUp(self):
		self.q = launcher.Launcher()

	def test_stop_calls_subprocess_stdin_write_with_stop_str(self):
		global stdin_write_arg1
		stdin_write_arg1 = None
		expected = "stop\r".encode()
		subprocess.Popen = FakeSubprocessPopen
		self.q.launch()
		self.q.stop()
		self.assertEqual(expected, stdin_write_arg1)

class LauncherQuitTestCase(unittest.TestCase):

	def setUp(self):
		self.q = launcher.Launcher()

	def test_quit_calls_subprocess_stdin_write_with_quit_str(self):
		global stdin_write_arg1
		stdin_write_arg1 = None
		expected = "quit\r".encode()
		subprocess.Popen = FakeSubprocessPopen
		self.q.launch()
		self.q.quit()
		self.assertEqual(expected, stdin_write_arg1)

if __name__ == "__main__":
	unittest.main()
