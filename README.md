# pyqemu

A Python 3 library to control Qemu.

## Quick Start

```
git clone git@gitlab.com:Lazlo/pyqemu
cd pyqemu
sudo ./setup.sh
make test
make package
sudo pip3 install --system dist/*.whl
./demo.sh
```

## TOOD

 * implement support for monitor control via telnet

### Bugs

 * ~~calls to Launcher.cont() cause main thread to block~~
