#!/usr/bin/python3

from pyqemu import Launcher
from time import sleep

def main():
	continue_after_nticks = 5
	stop_after_nticks = 20
	quit_after_nticks = 25
	nticks = 0
	l = Launcher()
	l.launch()
	while True:
		print("alive")
		if nticks == continue_after_nticks:
			print("requesting qemu to continue ...")
			l.cont()
		if nticks == stop_after_nticks:
			print("requesting qemu to stop ...")
			l.stop()
		if nticks == quit_after_nticks:
			print("requesting qemu to quit ...")
			l.quit()
			return
		nticks += 1
		sleep(0.5)

if __name__ == '__main__':
	main()
