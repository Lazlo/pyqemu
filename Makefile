test_cmd=python3 -m xmlrunner discover -p"*_test.py" -s pyqemu
pkg_cmd=python3 setup.py bdist_wheel sdist

test:
	$(test_cmd)
vtest:
	$(test_cmd) -v
package:
	$(pkg_cmd)
clean:
	$(RM) -rf */__pycache__
	$(RM) TEST-*.xml
